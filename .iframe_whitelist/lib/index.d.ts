import { JupyterFrontEndPlugin } from '@jupyterlab/application';
/**
 * Initialization data for the iframe-whitelist extension.
 */
declare const plugin: JupyterFrontEndPlugin<void>;
export default plugin;
