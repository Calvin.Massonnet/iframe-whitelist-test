import { IRenderMimeRegistry, markdownRendererFactory } from '@jupyterlab/rendermime';
import { PathExt } from '@jupyterlab/coreutils';
const fs = require('fs').promises;
/**
 * Asynchronously loads the list of whitelisted domains.
 */
async function loadWhitelistedDomains() {
    const filePath = PathExt.join(process.env.HOME || '', '.binder', 'iframe_whitelist.txt');
    try {
        const data = await fs.readFile(filePath, { encoding: 'utf-8' });
        const domains = new Set(data.split('\n').map((line) => line.trim()).filter((line) => line !== ''));
        return domains;
    }
    catch (error) {
        // Return an empty set if there's an error
        console.warn('iframe-whitelist: Could not load the whitelisted domains file: ', error);
        return new Set();
    }
}
/**
 * Modify the content of a Markdown cell to whitelist iframes from specific domains.
 */
function transformMarkdownWithWhitelistedIframes(markdown, whitelistedDomains) {
    // Regex to find iframe tags
    const iframeRegex = /<iframe\s+.*?\s+src="([^"]+)".*?>.*?<\/iframe>/gi;
    return markdown.replace(iframeRegex, (match, src) => {
        const url = new URL(src);
        if (whitelistedDomains.has(url.hostname)) {
            // Allow iframe
            return match;
        }
        else {
            // Remove iframe or replace with a placeholder
            return '';
        }
    });
}
/**
 * Activation function for the extension.
 */
function activate(app, renderMime) {
    console.log('iframe-whitelist: Extension activated!');
    const originalFactory = markdownRendererFactory;
    const newFactory = {
        ...originalFactory,
        safe: false,
        createRenderer: (options) => {
            const renderer = originalFactory.createRenderer(options);
            const originalRenderMime = renderer.renderModel.bind(renderer);
            renderer.renderModel = async (model) => {
                // Load the list of authorized domains
                const whitelistedDomains = await loadWhitelistedDomains();
                const data = model.data['text/markdown'];
                const transformedData = transformMarkdownWithWhitelistedIframes(data, whitelistedDomains);
                model.setData({ data: { ...model.data, 'text/markdown': transformedData } });
                return originalRenderMime(model);
            };
            return renderer;
        },
    };
    renderMime.removeMimeType('text/markdown');
    renderMime.addFactory(newFactory, 0);
}
/**
 * Initialization data for the iframe-whitelist extension.
 */
const plugin = {
    id: '${EXTENSION_ID}:plugin',
    autoStart: true,
    requires: [IRenderMimeRegistry],
    activate: activate
};
export default plugin;
